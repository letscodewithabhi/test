<?php

Route::get('/', function () {
    return view('welcome');
});
Route::resource('user', UserController::class);
Route::resource('album', AlbumController::class);