<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlbumTable extends Migration {

	public function up()
	{
		Schema::create('album', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->softDeletes();
			$table->string('title');
			$table->string('description');
			$table->string('img');
			$table->date('date');
			$table->integer('user_id');
			$table->boolean('featured');
		});
	}

	public function down()
	{
		Schema::drop('album');
	}
}