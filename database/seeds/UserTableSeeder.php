<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {

	public function run()
	{
		// addUser
		\App\User::create(array(
				'name' => "Nick Reynolds",
				'email' => "nick.reynolds@domain.co",
				'phone' => 555-555-5555,
				'bio' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
				'profile_picture' => "img/profile.jpg"
			));
	}
}