<?php

use Illuminate\Database\Seeder;

class AlbumTableSeeder extends Seeder {

    public function run()
    {

        \App\Album::create(array(
            'title' => "Nandhaka Pieris",
            'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            'img' => "img/landscape1.jpeg",
            'date' => "2015-05-01",
            'featured' => true,
            'user_id' => 1
        ));

        \App\Album::create(array(
            'title' => "New West Calgary",
            'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            'img' => "img/landscape2.jpeg",
            'date' => "2016-05-01",
            'featured' => false,
            'user_id' => 1
        ));

        \App\Album::create(array(
            'title' => "Australian Landscape",
            'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            'img' => "img/landscape3.jpeg",
            'date' => "2015-02-02",
            'featured' => false,
            'user_id' => 1
        ));

        \App\Album::create(array(
            'title' => "Halvergate Marsh",
            'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            'img' => "img/landscape4.jpeg",
            'date' => "2014-04-01",
            'featured' => true,
            'user_id' => 1
        ));

        \App\Album::create(array(
            'title' => "Rikkis Landscape",
            'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            'img' => "img/landscape5.jpeg",
            'date' => "2010-9-01",
            'featured' => false,
            'user_id' => 1
        ));

        \App\Album::create(array(
            'title' => "Kiddi Kristjans Iceland",
            'description' => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            'img' => "img/landscape6.jpeg",
            'date' => "2015-07-21",
            'featured' => true,
            'user_id' => 1
        ));
    }
}