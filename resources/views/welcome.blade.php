<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        * {
            box-sizing: border-box;
        }
        body {
            background-color: #f1f1f1;
            padding: 20px;
            font-family: Arial;
            position: absolute;
            top: 0; bottom: 0; left: 0; right: 0;
            height: 100%;
        }
        body:before {
            content: "";
            position: absolute;
            background: url("img/landscape3.jpeg");
            background-size: cover;
            z-index: -1; /* Keep the background behind the content */
            height: 20%; width: 20%; /* Using Glen Maddern's trick /via @mente */

            /* don't forget to use the prefixes you need */
            transform: scale(5);
            transform-origin: top left;
            filter: blur(10px);
        }
        .main {
            max-width: 1000px;
            margin: auto;
            /*background-color: #EEEEEE;*/
        }
        h1 {
            font-size: 50px;
            word-break: break-all;
        }
        .row {
            margin: 8px -16px;
        }
        .row, .row > .column-width-25 {
            padding: 8px;
        }
        .width-25 {
            float: left;
            width: 25%;
        }
        .float-left{
            float: left;
        }
        .width-100{
            width: 100%;
        }
        .width-80{
            width: 80%;
        }
        .width-60{
            width: 60%;
        }
        .width-33{
            width: 33%;
        }
        .width-20{
            width: 20%;
        }
        .width-15{
            width: 15%;
        }
        .width-10{
            width: 10%;
        }
        .width-5{
            width: 5%;
        }
        .height-152px{
            height: 152px;
        }
        .width-132px{
            width: 132px;
        }
        .width-260px{
            width: 260px;
        }
        .circular-image{
            border-radius: 50%;
        }
        .inline-block{
            display: inline-block; !important;
        }
        .colour-d966ff{
            color: #d966ff;
        }
        .row:after {
            content: "";
            display: table;
            clear: both;
        }
        .content {
            background-color: white;
            padding: 10px;
        }
        @media screen and (max-width: 900px) {
            .column-width-33 {
                width: 50%;
            }
        }
        @media screen and (max-width: 600px) {
            .column-width-33 {
                width: 100%;
            }
        }
        .card{
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            transition: 0.3s;
        }
        .container{
            padding: 2px 16px;
        }
        .font-bold{
            font-weight: bold;
        }
        .font-24px{
            font-size: 24px;
        }
        .colour-b3b3b3{
            color: #b3b3b3;
        }
        .float-right{
            float: right;
        }
        .bottom-left{
            position: relative;
            bottom: 8px;
            left: 16px;
        }
        .padding-top-5{
            padding-top: 5px;
        }
        .padding-right-5{
            padding-right: 5px;
        }
        .padding-left-5{
            padding-left: 5px;
        }
        .padding-bottom-5{
            padding-bottom: 5px;
        }
        .padding-bottom-10{
            padding-bottom: 10px;
        }
        .padding-top-10{
            padding-top: 10px;
        }
        .padding-left-10{
            padding-left: 10px;
        }
        .padding-right-10{
            padding-right: 10px;
        }
        .padding-bottom-20{
            padding-bottom: 20px;
        }
        .bg-white{
            background-color: #EEEEEE;
        }
        .color-red{
            color: red;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(window).load(function () {
            $.ajax({
                type: 'GET',
                url: 'user/1',
                success: function (data) {
                    //User Profile
                    $('#profilePic').attr("src", data.profile_picture);
                    $('#userName').append(data.name);
                    $('#bio').append(data.bio);
                    $('#phone').append(data.phone);
                    $('#email').append(data.email);

                    $.each(data.album, function (key, val) {

                        if(key == 0){
                            $('#albumPic').attr("src", val.img);
                            $('#albumTitle').append(val.title);
                            $('#albumDescription').append(val.description);
                            $('#albumDate').append(val.date);
                            if(val.featured == 1){
                                $('#featured').append('&#10084;');
                            }
                        }else{
                            var $clone = $('#albumItem').clone();
                            $clone.find('#albumPic').prop('src', val.img);
                            $clone.find('#albumTitle').text(val.title);
                            $clone.find('#albumDescription').text(val.description);
                            $clone.find('#albumDate').text(val.date);
                            if(val.featured == 1){
                                $clone.find('#featured').html('&#10084;');
                            }else{
                                $clone.find('#featured').remove();
                            }
                            $clone.appendTo('#albumRow');
                        }
                    });
                },
                error: function() {
                    console.log(data);
                }
            });
        });
    </script>
</head>
<body>
<div class="main">
    <div class="width-100 card padding-bottom-10 padding-top-10 padding-left-10 padding-right-10 bg-white">
        <div class="width-15 inline-block">
            <img id="profilePic" src="" class="circular-image height-152px width-132px">
        </div>
        <div class="width-60 inline-block">
            <div class="width-100">
                <span id="userName" class="font-bold font-24px"></span>
            </div>
            <br>
            <div class="width-100">
                <div class="width-100">
                    <div>
                        <label class="colour-b3b3b3 font-bold">Bio</label>
                    </div>
                    <div>
                        <span id="bio" class="colour-b3b3b3"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="width-20 inline-block">
            <div class="width-100">
                <div>
                    <label>Phone</label>
                </div>
                <div>
                    <span id="phone" class="colour-d966ff"></span>
                </div>
            </div>
            <div class="width-100">
                <div>
                    <label>Email</label>
                </div>
                <div>
                    <span id="email" class="colour-d966ff"></span>
                </div>
            </div>
        </div>
    </div>
    <p>Check for resposive behaviour of this page</p>
    <div id="albumRow">
        <div id="albumItem" class="width-33 inline-block padding-right-5 padding-left-5 padding-bottom-5 padding-top-5">
            <div class="card">
                <img id="albumPic" src="" class="width-100">
                <div id="albumPicContent" class="container bg-white">
                    <h3 id="albumTitle"></h3>
                    <p id="albumDescription"></p>
                    <div id="imgFooter" class="padding-bottom-20">
                        <span id="featured" class="color-red" style="float: left"></span>
                        <span id="albumDate" class="colour-b3b3b3" style="float: right"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>